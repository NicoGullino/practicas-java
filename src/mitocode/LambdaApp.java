package mitocode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LambdaApp {
	
	public void ordenar() {
		List <String> lista = new ArrayList<>();
		lista.add("MitoCode");
		lista.add("Code");
		lista.add("Mito");
		
		Collections.sort(lista, (String s1, String s2) -> s1.compareTo(s2));
		
		for (String elemento: lista) {
			System.out.println(elemento);
		}
	}
	
	public static void main(String[] args) {
		LambdaApp app = new LambdaApp();
		app.ordenar();
	}
	
}
